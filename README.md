# Soulwinder
Private (possibly-to-be-public) plugin for upcoming Minecraft server project based on the webtoon with the same name.

[![pipeline status](https://gitlab.com/bdotsamir/soulwinder/badges/main/pipeline.svg)](https://gitlab.com/bdotsamir/soulwinder/-/commits/main)

## Download
You'll want to download the one *without* the `original-` prefix.
* [Bleeding Edge](https://gitlab.com/bdotsamir/soulwinder/-/jobs/artifacts/main/browse/target/?job=build)
* Latest Release (coming soon...)

## Dependencies
* [WorldGuard](https://dev.bukkit.org/projects/worldguard)
* [WorldEdit](https://dev.bukkit.org/projects/worldedit)
* [ProtocolLib](https://www.spigotmc.org/resources/protocollib.1997/)
* [Holograms Plugin](https://www.spigotmc.org/resources/decentholograms-1-8-1-19-4-papi-support-no-dependencies.96927/)?

## Todo:
* [x] House all commands under `/sw`

[//]: # (* [ ] Avatars:)

[//]: # (  * [x] Actually spawn the little freaks &#40;currently ArmorStands&#41;)

[//]: # (  * [ ] Make them Do Stuff)

[//]: # (  * [ ] Migrate to citizens api?)

[//]: # (  * [ ] Make them follow players?)

[//]: # (  * [x] Make them wear leather armor colored by their class)
[//]: # (* [ ] Summoning fields:)

[//]: # (  * [x] Create the damn things)

[//]: # (  * [x] Save the blocks they may be overlapping, so they can be replaced when the field is destroyed)

[//]: # (  * [x] Make avatars only available inside of summoning fields)

[//]: # (  * [x] Integrate with WorldGuard to check for summoning field?)

[//]: # (  * [ ] Make fields of varying sizes)

[//]: # (  * [ ] Integrate with WorldEdit wand to place corners)

[//]: # (  * [ ] Add Citizens NPCs with predefined paths around the school. They have the ability to create summoning fields in specific subjects.)
* [ ] Create items

[//]: # (  * [ ] The iron bracelet)
[//]: # (* [x] Create classes)

[//]: # (  * [x] F: Red)

[//]: # (  * [x] E: Yellow)

[//]: # (  * [x] D: Lime Green)

[//]: # (  * [x] C: Green)

[//]: # (  * [x] B: Blue)

[//]: # (  * [x] A: Purple)
[//]: # (* [ ] Summoner Test Wars)

[//]: # (  * [ ] Damage done has GOT to be better than just a random number ranging from 1 to score in subject.)

[//]: # (  * Maybe on each turn, there could be a question asked from the subject, and if they get it right they deal more damage?)

[//]: # (  * On building up the avatar's score for the next ST war, the question on the test can be either an "offensive" question or a "defensive" question. Offensive questions deal more damage, defensive questions allow the avatar to *take* more damage from the opponent &#40;negate opposing damage&#41;.)

[//]: # (  * See ChatGPT details.)

[//]: # (  * Practice ST wars could contribute to offensive / defensive points.)

[//]: # (  * Maybe implement a skill tree!)

[//]: # (  * [ ] Test wars. Literally take a written test. Highest score wins. Avatars not needed.)
* [ ] Skill tree (possibly)
  * Offensive skills
  * Defensive skills

[//]: # (Notes from the show:)

[//]: # (* Players cannot move beyond the barriers of another class's avatars)

[//]: # (* Avatars follow players if outside range x)

[//]: # (* Name tags above avatars show damage taken)

[//]: # (* Exams have unlimited questions thus unlimited score, so you can answer as many questions you want within the time limit)