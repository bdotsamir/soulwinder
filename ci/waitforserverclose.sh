#!/bin/bash

ROOT_DIR=/builds/bdotsamir/minecraft

# We're constantly checking the logs for the server to be ready.
# This is signified by the line "Closing server"
while true; do
  if grep "Closing Server" $ROOT_DIR/logs/latest.log;
  then
    break;
  fi

  sleep 1;
done