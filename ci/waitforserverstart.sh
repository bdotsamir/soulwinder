#!/bin/bash

ROOT_DIR=/builds/bdotsamir/minecraft

# We're constantly checking the logs for the server to be ready.
# This is signified by the line "Done! (69.420s) For help, type "help""
while true; do
  if grep "For help, type \"help\"" $ROOT_DIR/logs/latest.log;
  then
    break;
  fi

  sleep 1;
done