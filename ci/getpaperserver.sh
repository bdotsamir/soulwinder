#!/bin/bash

# original script by https://github.com/astorks/PaperMC.sh/blob/master/papermc.sh

apt-get update -y
apt-get install -y jq curl screen

ROOT_DIR=/builds/bdotsamir

mkdir -pv $ROOT_DIR/minecraft/plugins

# Get required plugin dependencies
PROTOCOLLIB="https://api.spiget.org/v2/resources/1997/download"
WORLDGUARD="https://mediafilez.forgecdn.net/files/4554/903/worldguard-bukkit-7.0.8-dist.jar"
WORLDEDIT="https://mediafilez.forgecdn.net/files/4445/117/worldedit-bukkit-7.2.14.jar"

echo "Getting required plugins..."
wget -O $ROOT_DIR/minecraft/plugins/ProtocolLib.jar $PROTOCOLLIB
echo "Downloaded ProtocolLib"
wget -O $ROOT_DIR/minecraft/plugins/WorldGuard.jar $WORLDGUARD
echo "Downloaded WorldGuard"
wget -O $ROOT_DIR/minecraft/plugins/WorldEdit.jar $WORLDEDIT
echo "Downloaded WorldEdit"

# Get the paper file
PAPERMC_VERSION=1.19.4
LATEST_BUILD=$(curl -s "https://papermc.io/api/v2/projects/paper/versions/${PAPERMC_VERSION}" | jq '.builds[-1]')
LATEST_DOWNLOAD=$(curl -s "https://papermc.io/api/v2/projects/paper/versions/${PAPERMC_VERSION}/builds/${LATEST_BUILD}" | jq '.downloads.application.name' -r)
echo -----------------
echo $PAPERMC_VERSION#$LATEST_BUILD
echo -----------------
PAPERMC_DOWNLOAD_URL="https://papermc.io/api/v2/projects/paper/versions/${PAPERMC_VERSION}/builds/${LATEST_BUILD}/downloads/${LATEST_DOWNLOAD}"
curl -s -o $ROOT_DIR/minecraft/paperclip.jar ${PAPERMC_DOWNLOAD_URL}

echo "eula=true" | tee $ROOT_DIR/minecraft/eula.txt
