
  ██╗    ██╗ █████╗ ██████╗ ███╗   ██╗██╗███╗   ██╗ ██████╗ ██╗
  ██║    ██║██╔══██╗██╔══██╗████╗  ██║██║████╗  ██║██╔════╝ ██║
  ██║ █╗ ██║███████║██████╔╝██╔██╗ ██║██║██╔██╗ ██║██║  ███╗██║
  ██║███╗██║██╔══██║██╔══██╗██║╚██╗██║██║██║╚██╗██║██║   ██║╚═╝
  ╚███╔███╔╝██║  ██║██║  ██║██║ ╚████║██║██║ ╚████║╚██████╔╝██╗
   ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═══╝╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚═╝

EVERYTHING IN THIS FOLDER IS NOT MEANT TO BE TOUCHED BY HUMAN HANDS.

IF YOU ARE _NOT_ A DEVELOPER, YOU ARE IN THE WRONG PLACE.

IF YOU _ARE_ A DEVELOPER, YOU'RE PROBABLY _STILL_ IN THE WRONG PLACE.
YOU MIGHT BE LOOKING FOR THE SOURCE CODE.
• HTTPS://GITLAB.COM/BDOTSAMIR/BAKAANDTEST

The files in this directory contain a LOT of numbers. A lot of it will
seem arbitrary, but I assure you, it is not. These values are CRITICAL
for the plugin to function properly. Things like test scores, server
member classes, random biases, and more are all stored in these
files. IF YOU CHANGE ANYTHING, YOU WILL BREAK THE PLUGIN, OR CAUSE
IT TO ACT IN VERY ERRATIC WAYS.

ALL THE NECESSARY CONFIGURATION IS DONE VIA THE CONFIG.YML FILE.
I CAN ASSURE YOU THERE IS NOTHING HERE FOR YOU TO SEE.

If ANYTHING is changed by hand, and it starts to act wrongly, you'll need
to DELETE EVERYTHING in this folder and START OVER. Attempting to fix it
is VERY hit or miss.
YOU WILL ALSO NOT GET ANY SUPPORT FROM ME OR THE DEVS IF SOMETHING BREAKS
BECAUSE YOU CHANGED A VALUE HERE.
