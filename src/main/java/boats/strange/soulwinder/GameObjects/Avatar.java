package boats.strange.soulwinder.GameObjects;

import boats.strange.soulwinder.Soulwinder;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import java.util.concurrent.atomic.AtomicInteger;

public class Avatar {

    private final Student student;
    private final Location location;
    private Color classColor;

    private ArmorStand entity;

    private BukkitTask particleCreator;

    public Avatar(Student student, Location location) {
        this.student = student;
        this.location = location;
    }

    public void summon() {
        ArmorStand as = this.student.player.getWorld().spawn(location, ArmorStand.class);
        this.entity = as;
        // Make it invisible until we set everything
        as.setVisible(false);

        this.classColor = this.student.getClassColor();

        ItemStack skull = new ItemStack(Material.PLAYER_HEAD);
        SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
        skullMeta.setOwningPlayer(this.student.player);
        skull.setItemMeta(skullMeta);

        as.setArms(true);
        as.setBasePlate(false);
        as.setSmall(true);
        EntityEquipment asEquipment = as.getEquipment();

        ItemStack leatherChestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
        leatherChestplate.editMeta(LeatherArmorMeta.class, meta -> meta.setColor(this.classColor));

        asEquipment.setHelmet(skull, true);
        asEquipment.setChestplate(leatherChestplate, true);
        asEquipment.setLeggings(new ItemStack(Material.IRON_LEGGINGS), true);
        asEquipment.setBoots(new ItemStack(Material.IRON_BOOTS), true);
        as.setItem(EquipmentSlot.HAND, new ItemStack(Material.IRON_SWORD));

        // Make it glow
        as.setGlowing(true);

        // Can't "hit" it
        as.setInvulnerable(true);

        as.setVisible(true);

        this.spawnParticles();
    }

    public void destroy() {
        this.entity.remove();
        this.particleCreator.cancel();
    }

    private void spawnParticles() {
        BukkitScheduler scheduler = this.student.player.getServer().getScheduler();
        Soulwinder plugin = (Soulwinder) this.student.player.getServer().getPluginManager().getPlugin("BakaAndTest");
        assert plugin != null;

        AtomicInteger i = new AtomicInteger();
        this.particleCreator = scheduler.runTaskTimer(plugin, () -> {
            Location avatarLocation = this.entity.getLocation();
            Location playerLocation = this.student.player.getLocation();

            // Spawn particles in a circle around the avatar
            double angle = i.get() * Math.PI / 180;
            double ax = Math.cos(angle) * 0.5; // avatar
            double az = Math.sin(angle) * 0.5;
            double px = Math.cos(angle) * 0.9; // player
            double pz = Math.sin(angle) * 0.9;

            double yTilt = 0.2 * Math.sin(angle);

            avatarLocation.add(ax, 0.5 + yTilt, az);
            avatarLocation.getWorld().spawnParticle(
                    Particle.REDSTONE, avatarLocation, 2,
                    new Particle.DustOptions(
                            this.classColor, 1));
            avatarLocation.subtract(ax, 0, az);

            // Spawn matching particles in a circle around the player
            playerLocation.add(px, 1 + yTilt, pz);
            playerLocation.getWorld().spawnParticle(
                    Particle.REDSTONE, playerLocation, 2,
                    new Particle.DustOptions(
                            this.classColor, 1));
            playerLocation.subtract(px, 0, pz);

            i.addAndGet(20);
            if(i.get() >= 360) {
                i.set(0);
            }
        }, 0L, 1L);
    }

    public Location getLocation() {
      return this.location;
    }
}
