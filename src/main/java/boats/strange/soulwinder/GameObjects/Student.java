package boats.strange.soulwinder.GameObjects;

import boats.strange.soulwinder.Soulwinder;
import boats.strange.soulwinder.GameObjects.SkillTree.SkillTreeInventory;
import boats.strange.soulwinder.util.CustomConfig;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.Color;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

public class Student {

  private final Soulwinder plugin;
  public final Player player;
  public final CustomConfig studentConfig;

  private ClassLetter classLetter = ClassLetter.UNSET;
  private ArrayList<String> skills = new ArrayList<>();

  public enum ClassLetter {
    CLASS_A,
    CLASS_B,
    CLASS_C,
    CLASS_D,
    CLASS_E,
    CLASS_F,
    UNSET
  }

  public static final Color COLOR_A = Color.fromRGB(108, 71, 255);
  public static final Color COLOR_B = Color.fromRGB(25, 144, 255);
  public static final Color COLOR_C = Color.fromRGB(120, 245, 122);
  public static final Color COLOR_D = Color.fromRGB(177, 247, 156);
  public static final Color COLOR_E = Color.fromRGB(255, 169, 31);
  public static final Color COLOR_F = Color.fromRGB(255, 0, 0);

  public Student(Soulwinder plugin, Player player) {
    this.plugin = plugin;
    this.player = player;

    this.studentConfig = new CustomConfig(this.plugin, this.player.getUniqueId() + ".yml", true);
    this.studentConfig.getConfig().addDefault("class", "unset");
    this.studentConfig.getConfig().addDefault("skills", new ArrayList<>());
    this.studentConfig.getConfig().options().copyDefaults(true);
    this.studentConfig.saveConfig();
//    this.save();

    this.deserialize();
  }

  private void deserialize() {
    // Read the class letter from the config.
    String classString = this.studentConfig.getConfig().getString("class");
    // We never have to worry about it not being set, because there's a default.
    assert classString != null;
    this.classLetter = stringToClassLetter(classString);

    // Read the skills the student has unlocked
    this.skills = (ArrayList<String>) this.studentConfig.getConfig().getStringList("skills");
  }

  private void serialize() {
    // Write the class letter to the config.
    String classString = classLetterToString(this.classLetter);
    this.studentConfig.getConfig().set("class", classString);

    // Write the skills the student has unlocked
    this.studentConfig.getConfig().set("skills", this.skills);

    this.studentConfig.saveConfig();
  }

  public void setClassLetter(@NotNull ClassLetter classletter) {
    this.classLetter = classletter;
  }
  public @NotNull ClassLetter getClassLetter() {
    return this.classLetter;
  }

  public @Nullable Color getClassColor() {
    Color color;

    switch(this.classLetter) {
      case CLASS_A -> color = COLOR_A;
      case CLASS_B -> color = COLOR_B;
      case CLASS_C -> color = COLOR_C;
      case CLASS_D -> color = COLOR_D;
      case CLASS_E -> color = COLOR_E;
      case CLASS_F -> color = COLOR_F;
      default -> color = null;
    }

    return color;
  }

  public static @NotNull TextColor colorToTextColor(@Nullable Color color) {
    if(color == null) {
      return NamedTextColor.RED;
    } else {
      return TextColor.color(color.asRGB());
    }
  }

  public void save() {
    this.serialize();
    this.plugin.getLogger().info("Saved student " + this.player.getName() + " to file.");
  }

  public static ClassLetter stringToClassLetter(@NotNull String letter) {
    ClassLetter classLetter;
    switch(letter) {
      case "A" -> classLetter = ClassLetter.CLASS_A;
      case "B" -> classLetter = ClassLetter.CLASS_B;
      case "C" -> classLetter = ClassLetter.CLASS_C;
      case "D" -> classLetter = ClassLetter.CLASS_D;
      case "E" -> classLetter = ClassLetter.CLASS_E;
      case "F" -> classLetter = ClassLetter.CLASS_F;
      case "unset" -> classLetter = ClassLetter.UNSET;
      default -> throw new RuntimeException("Invalid class letter");
    }
    return classLetter;
  }

  public static String classLetterToString(@NotNull ClassLetter letter) {
    String classLetter;
    switch(letter) {
      case CLASS_A -> classLetter = "A";
      case CLASS_B -> classLetter = "B";
      case CLASS_C -> classLetter = "C";
      case CLASS_D -> classLetter = "D";
      case CLASS_E -> classLetter = "E";
      case CLASS_F -> classLetter = "F";
      case UNSET -> classLetter = "unset";
      default -> throw new RuntimeException("You should never see this. Java's enum checking is weird.");
    }
    return classLetter;
  }

  public ArrayList<String> getSkills() {
    return this.skills;
  }

  public boolean isUnlocked(@NotNull SkillTreeInventory.SkillTreeNode node) {
    return this.getSkills().contains(node.id);
  }
}
