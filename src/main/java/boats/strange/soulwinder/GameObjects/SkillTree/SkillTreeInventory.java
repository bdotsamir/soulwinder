package boats.strange.soulwinder.GameObjects.SkillTree;

import boats.strange.soulwinder.GameObjects.Student;
import com.google.gson.Gson;
import boats.strange.soulwinder.Soulwinder;
import boats.strange.soulwinder.util.SkullUtils;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

// Thank you, https://docs.papermc.io/paper/dev/custom-inventory-holder
public class SkillTreeInventory implements InventoryHolder {

  public static class SkillTreeNode {
    public String id;
    public String name;
    public String description;
    public String item;
    public SkillTreeNode[] children;

    // Empty constructor for GSON to fill
    public SkillTreeNode() { }
  }

  private final Soulwinder plugin;
  private final Student student;
  private final SkullUtils skullUtils;

  private final Inventory inventory;

  public final SkillTreeNode headNode;
  private SkillTreeNode currentNode;
  private final Stack<SkillTreeNode> nodeStack = new Stack<>();

  private final HashMap<Integer, SkillTreeNode> invSlotToNodeMap = new HashMap<>();

  public SkillTreeInventory(@NotNull Soulwinder plugin, @NotNull Student student) {
    this.plugin = plugin;
    this.student = student;

    // Create an Inventory with 9 slots, `this` here is our InventoryHolder.
    this.inventory = plugin.getServer().createInventory(this, 54);

    this.skullUtils = new SkullUtils(plugin);

    Gson gson = new Gson();
    InputStream skillTreeJsonFile = this.plugin.getResource("skilltree.json");
    if(skillTreeJsonFile == null) {
      throw new RuntimeException("Could not find skilltree.json");
    }
    Reader reader = new InputStreamReader(skillTreeJsonFile, StandardCharsets.UTF_8);
    SkillTreeNode node = gson.fromJson(reader, SkillTreeNode.class);
    this.headNode = node;
    this.currentNode = node;

    this.populateInventory();
  }

  @Override
  public @NotNull Inventory getInventory() {
    return this.inventory;
  }

  /**
   * Clear the inventory if it's not empty, then populate it with the current skill tree's data.
   */
  public void populateInventory() {
    this.invSlotToNodeMap.clear(); // First, clear the map, since we're putting in new data.
    this.inventory.clear(); // Also, clear the inventory for re-population.

    ItemStack currentNodeItem = this.getSkillTreeItemStack(this.currentNode);
    this.inventory.setItem(13, currentNodeItem);
    this.invSlotToNodeMap.put(13, this.currentNode);

    // 27 - 35
    SkillTreeNode[] children = this.currentNode.children;
    if(children.length != 0) {
      if(children.length > 9) {
        throw new RuntimeException("Skill tree node has more than 9 children");
      }

      int around = 0;
      int middleSlot = 31;
      for(int i = 0; i < children.length; i++) {
        SkillTreeNode childNode = children[i];
        ItemStack childNodeItem = this.getSkillTreeItemStack(childNode);

        int slot = i % 2 == 0 ? middleSlot - around : middleSlot + around;
        this.inventory.setItem(slot, childNodeItem);
        this.invSlotToNodeMap.put(slot, children[i]);

        if(children[i].children.length != 0) {
          ItemStack downSkull = this.skullUtils.getSkull(SkullUtils.DOWN_ARROW);
          downSkull.editMeta(meta -> meta.displayName(Component.text("Next")));
          this.inventory.setItem(slot + 9, downSkull);
        }

        if(i % 2 == 0) {
          around++;
        }

      }
    }

    // If we're not at the head node,
    if(!this.nodeStack.isEmpty()) {
      ItemStack upSkull = this.skullUtils.getSkull(SkullUtils.UP_ARROW);
      upSkull.editMeta(meta -> meta.displayName(Component.text("Previous")));
      // Set an up arrow.
      this.inventory.setItem(4, upSkull);
    }
  }

  private ItemStack getSkillTreeItemStack(SkillTreeNode node) {
    Material material;
    SkillTreeNode dependsOnThisNode = this.getWhichNodeThisDependsOn(node);
    if(dependsOnThisNode != null && !this.student.isUnlocked(dependsOnThisNode)) {
      material = Material.BARRIER;
    } else {
      material = Material.matchMaterial(node.item);
    }

    if(material == null) {
      throw new RuntimeException("Could not find material " + node.item);
    }

    ItemStack item = new ItemStack(material);
    item.editMeta(meta -> {
      // Just in case the item has any additional attributes like attack speed, etc.
      meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);

      List<Component> lore = new ArrayList<>();

      meta.displayName(Component.text(node.name)
              .decoration(TextDecoration.ITALIC, false)
      );
      lore.add(Component.text(node.description)
              .decoration(TextDecoration.ITALIC, false)
      );

      boolean isUnlocked = this.student.getSkills().contains(node.id);
      if (isUnlocked) {
        // If the skill has been unlocked, add an enchantment glint.
        meta.addEnchant(Enchantment.LUCK, 1, true);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
      }

      if(dependsOnThisNode != null && !this.student.isUnlocked(dependsOnThisNode)) {
        lore.add(Component.text("You must unlock " + dependsOnThisNode.name + " first!", NamedTextColor.RED)
                .decoration(TextDecoration.ITALIC, false)
        );
      }

      meta.lore(lore);
    });
    return item;
  }

  public @Nullable SkillTreeNode getNodeAtSlot(int slot) {
    if(this.invSlotToNodeMap.containsKey(slot)) {
      return this.invSlotToNodeMap.get(slot);
    }
    // else,
    return null;
  }

  public @Nullable SkillTreeNode goUp() {
    if(this.nodeStack.isEmpty()) {
      return null; // We're already at the top of the tree!
    }
    this.currentNode = this.nodeStack.pop();
    return this.currentNode;
  }

  public void goDown(@NotNull SkillTreeNode node) {
    this.nodeStack.push(this.currentNode);
    this.currentNode = node;
  }

  /**
   * Returns the node that needs to first be unlocked before this one can be unlocked.
   * If this node does not depend on any other node, returns null.
   * @param node The node to check.
   */
  public @Nullable SkillTreeNode getWhichNodeThisDependsOn(@NotNull SkillTreeNode node) {
    return this._recursiveNodeDepends(this.headNode, node);
  }

  private SkillTreeNode _recursiveNodeDepends(@NotNull SkillTreeNode headNode, @NotNull SkillTreeNode node) {
    for(SkillTreeNode childNode : headNode.children) {
      if(childNode.id.equals(node.id)) {
        return headNode;
      }
      SkillTreeNode result = this._recursiveNodeDepends(childNode, node);
      if(result != null) {
        return result;
      }
    }
    return null;
  }
}
