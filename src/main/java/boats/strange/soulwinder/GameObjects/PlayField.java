package boats.strange.soulwinder.GameObjects;

import com.google.gson.*;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.managers.RemovalStrategy;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import boats.strange.soulwinder.Soulwinder;
import boats.strange.soulwinder.util.MCResponse;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.util.HashMap;

@Deprecated
public class PlayField {

  // Transient marks a field as not-to-be-serialized
  private final transient Soulwinder plugin;

  private World world;
  private final Location location;
  private Player owner;

  private transient RegionManager regionManager;

  private final int xLength;
  private final int height;
  private final int zLength;

  private double topXCorner;
  private double topYCorner;
  private double topZCorner;
  // from these three values, I can then calculate out the bottom three values.

  // Blocks that accidentally got replaced when creating the field to then be
  // replaced later when the field is destroyed.
  private final HashMap<Location, Material> replacedBlocks = new HashMap<>();
  private final HashMap<Player, Avatar> avatars = new HashMap<>();

  private transient BukkitTask particleCreator;

  public PlayField(Soulwinder plugin, Location location, int xLength, int height, int zLength) {
    this.plugin = plugin;

    this.location = location;
    this.xLength = xLength;
    this.height = height;
    this.zLength = zLength;
  }

  public void create(@NotNull Player player) {
    this.owner = player;
//    this.location = player.getLocation();
    this.world = this.location.getWorld();
//    player.sendMessage(this.location.toString());

    // These three values are the "bottom" three values.
    double currentX = this.location.getX() - 1;
    double currentZ = this.location.getZ() - 1;
    double currentY = this.location.getY() - 1;

    double minX = currentX - Math.floor(xLength / 2d);
    double minZ = currentZ - Math.floor(zLength / 2d);
    double maxY = currentY + height;

    this.topXCorner = minX + xLength;
    this.topYCorner = maxY;
    this.topZCorner = minZ + zLength;

    for (double x = minX; x < minX + xLength; x++) {
      for (double z = minZ; z < minZ + zLength; z++) {
        Block block = this.world.getBlockAt((int) x, (int) maxY, (int) z);
//        this.plugin.getLogger().info(block.toString());
        // If we replaced a block that wasn't air, save it to be un-replaced later.
        if (block.getType() != Material.AIR) {
          this.replacedBlocks.put(block.getLocation(), block.getType());
          this.plugin.getLogger().warning("Replaced block " + block);
        }

        block.setType(Material.TINTED_GLASS);
      }
    }

    BukkitScheduler scheduler = player.getServer().getScheduler();
    this.particleCreator = scheduler.runTaskTimer(this.plugin, () -> {

      for (double x = this.topXCorner; x > minX; x--) {
        for (double y = this.topYCorner; y > currentY; y--) {
          this.world.spawnParticle(Particle.ASH, x, y, topZCorner, 10);
          this.world.spawnParticle(Particle.ASH, x, y, topZCorner - zLength, 10);
        }
      }

      for (double z = this.topZCorner; z > minZ; z--) {
        for (double y = this.topYCorner; y > currentY; y--) {
          this.world.spawnParticle(Particle.ASH, topXCorner, y, z, 10);
          this.world.spawnParticle(Particle.ASH, topXCorner - xLength, y, z, 10);
        }
      }

    }, 0L, 5L);

    // create a worldguard region
    Location bottomCorner = new Location(this.world, minX, currentY, minZ);
    Location topCorner = new Location(this.world, this.topXCorner, this.topYCorner, this.topZCorner);
    this.createWorldGuardRegion(player, bottomCorner, topCorner);
  }

  private void createWorldGuardRegion(@NotNull Player player, @NotNull Location bottomCorner, @NotNull Location topCorner) {
    BlockVector3 min = BlockVector3.at(bottomCorner.getX(), bottomCorner.getY(), bottomCorner.getZ());
    BlockVector3 max = BlockVector3.at(topCorner.getX(), topCorner.getY(), topCorner.getZ());

    // Cool, so we've created the region. Now we need to save it.
    ProtectedRegion region = new ProtectedCuboidRegion("playfield" + player.getUniqueId(), min, max);

    // Save the region to the plugin's hashmap, so we can check for player location later.
    this.plugin.getRegionToFieldMap().put(region, this);

    // First we need to get the WorldGuard instance.
    WorldGuard worldGuard = this.plugin.getWorldGuard();
    // Then we get the object that contains all the regions
    RegionContainer container = worldGuard.getPlatform().getRegionContainer();
    // (but first we need to convert a bukkit-style world into a WorldEdit-style world)
    com.sk89q.worldedit.world.World worldEditWorld = BukkitAdapter.adapt(this.world);
    // Then we get the region manager for the current world
    RegionManager regions = container.get(worldEditWorld);
    if (regions == null) {
      player.sendMessage(MCResponse.error("Something went wrong. WorldGuard is not behaving properly. Try again later?"));
      return;
    }
    this.regionManager = regions;
    // Then finally add the region we created earlier to the region container
    regions.addRegion(region);
  }

  private void destroyWorldGuardRegion() {
    this.regionManager.removeRegion("playfield" + this.owner.getUniqueId(), RemovalStrategy.REMOVE_CHILDREN);
  }

  public void destroy() {
    // stop making particles. stop it right now.
    this.particleCreator.cancel();

    // `i` is an x coordinate. `j` is a z coordinate.
    for (int i = (int) (this.topXCorner - this.xLength); i < this.topXCorner; i++) {
      for (int j = (int) (this.topZCorner - this.zLength); j < this.topZCorner; j++) {
        Location location = new Location(this.world, i, this.topYCorner, j);
        if (this.replacedBlocks.containsKey(location)) {
          Material material = this.replacedBlocks.get(location);
          Block blockAtLocation = this.world.getBlockAt(location);
          blockAtLocation.setType(material);
        } else {
          Block block = this.world.getBlockAt(location);
          block.setType(Material.AIR);
        }
      }
    }

    this.plugin.getPlayerToFieldMap().remove(this.owner);
    this.destroyWorldGuardRegion();

    // Remove all avatars in the field
    for (Avatar avatar : this.avatars.values()) {
      avatar.destroy();
    }
  }

  public HashMap<Player, Avatar> getAvatars() {
    return this.avatars;
  }

  public void addAvatar(@NotNull Player player, @NotNull Avatar avatar) {
    this.avatars.put(player, avatar);
  }

  public String serialize() {
    Gson gson = new GsonBuilder()
            .serializeNulls()
            // This is a non-trivial class to serialize. We need a custom adapter.
            .registerTypeAdapter(PlayField.class, new PlayFieldSerializer())
            .setPrettyPrinting().create();
    return gson.toJson(this);
  }

//  public static PlayField deserialize() {
//
//  }

  // This is the custom serialization class.
  public static class PlayFieldSerializer implements JsonSerializer<PlayField> {
    public JsonElement serialize(PlayField src, Type typeOfSrc, JsonSerializationContext context) {
      JsonObject serializedObj = new JsonObject();

      serializedObj.add("owner",
              new JsonPrimitive(src.owner.getUniqueId().toString())
      );

      JsonArray location = new JsonArray();
      location.add(src.location.getX());
      location.add(src.location.getY());
      location.add(src.location.getZ());
      serializedObj.add("location", location);

      serializedObj.add("xLength", new JsonPrimitive(src.xLength));
      serializedObj.add("height", new JsonPrimitive(src.height));
      serializedObj.add("zLength", new JsonPrimitive(src.zLength));

      JsonArray avatars = new JsonArray();
      src.avatars.forEach(((player, avatar) -> {
        JsonArray avatarLocation = new JsonArray();
        avatarLocation.add(avatar.getLocation().getX());
        avatarLocation.add(avatar.getLocation().getY());
        avatarLocation.add(avatar.getLocation().getZ());

        JsonObject playerAvatarObj = new JsonObject();
        playerAvatarObj.add(player.getUniqueId().toString(), avatarLocation);

        avatars.add(playerAvatarObj);
      }));
      serializedObj.add("avatars", avatars);

      return serializedObj;
    }
  }

  // This class may be unnecessary!
  public static class PlayFieldDeserializer implements JsonDeserializer<PlayField> {
    public PlayField deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
      return null;
    }
  }

  public static class PlayFieldInstanceCreator implements InstanceCreator<PlayField> {
    public PlayField createInstance(Type type) {
      return null;
    }
  }
}
