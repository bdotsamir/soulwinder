package boats.strange.soulwinder.events;

import boats.strange.soulwinder.GameObjects.SkillTree.SkillTreeInventory;
import boats.strange.soulwinder.GameObjects.Student;
import com.destroystokyo.paper.profile.PlayerProfile;
import boats.strange.soulwinder.Soulwinder;
import boats.strange.soulwinder.util.SkullUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.jetbrains.annotations.NotNull;

public class InventoryClick implements Listener {

  private final Soulwinder plugin;

  public InventoryClick(Soulwinder plugin) {
    this.plugin = plugin;
  }

  @EventHandler
  public void onInventoryClick(org.bukkit.event.inventory.@NotNull InventoryClickEvent event) {
    Inventory inv = event.getInventory();
    if(!(inv.getHolder() instanceof SkillTreeInventory stInv)) {
      return;
    }

    event.setCancelled(true);

    // Only register if it's the custom inventory, not the player's inventory
    if(event.getClickedInventory() != stInv.getInventory()) {
      return;
    }

    ItemStack clickedItem = event.getCurrentItem();
    if(clickedItem == null) {
      return;
    }

    // Handle navigation of the skill tree (up or down)
    if(clickedItem.getType() == Material.PLAYER_HEAD) {
      SkullMeta meta = (SkullMeta) clickedItem.getItemMeta();
      PlayerProfile profile = meta.getPlayerProfile();
      if(profile == null) {
        throw new RuntimeException("Could not get player profile for skull");
      }
      String texture = profile.getProperties()
              .stream().filter(property -> property.getName().equals("textures"))
              .findFirst().orElseThrow().getValue();

      if(texture.equals(SkullUtils.DOWN_ARROW)) {
        SkillTreeInventory.SkillTreeNode newNode = stInv.getNodeAtSlot(event.getSlot() - 9);
        if(newNode == null) {
          throw new RuntimeException("Could not find node at slot " + (event.getSlot() - 9));
        }

        // Set the new current node,
        stInv.goDown(newNode);
        // then populate the inventory.

      }
      if(texture.equals(SkullUtils.UP_ARROW)) {
        // Set the new current node,
        stInv.goUp();
      }
    } else {
      // Handle skill selection
      SkillTreeInventory.SkillTreeNode selectedNode = stInv.getNodeAtSlot(event.getSlot());
      if(selectedNode == null) {
        throw new RuntimeException("Could not find node at slot " + event.getSlot());
      }

      Student student = this.plugin.students.get((Player) event.getWhoClicked());
      if(!student.isUnlocked(selectedNode)) {
        student.getSkills().add(selectedNode.id);
        student.save();
      }
    }

    // Repopulate the inventory with the new data
    stInv.populateInventory();
  }
}
