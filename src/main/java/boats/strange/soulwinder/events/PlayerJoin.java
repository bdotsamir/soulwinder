package boats.strange.soulwinder.events;

import boats.strange.soulwinder.GameObjects.Student;
import boats.strange.soulwinder.Soulwinder;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener {

  private final Soulwinder plugin;

  public PlayerJoin(Soulwinder plugin) {
    this.plugin = plugin;
  }

  @EventHandler
  public void onPlayerJoin(PlayerJoinEvent event) {
    Player player = event.getPlayer();

    boolean containsStudent = this.plugin.students.containsKey(player);
    // Should always return false, but just in case...
    if(!containsStudent) {
      this.plugin.getLogger().info("Creating student...");
      Student student = new Student(this.plugin, player);
      this.plugin.students.put(player, student);
    }
  }

}
