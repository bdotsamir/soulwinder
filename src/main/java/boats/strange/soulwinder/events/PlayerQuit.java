package boats.strange.soulwinder.events;

import boats.strange.soulwinder.Soulwinder;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuit implements Listener {

  private final Soulwinder plugin;

  public PlayerQuit(Soulwinder plugin) {
    this.plugin = plugin;
  }

  @EventHandler
  public void onPlayerLeave(PlayerQuitEvent event) {
    this.plugin.students.remove(event.getPlayer());
    this.plugin.getLogger().info("Removed player from students list");
  }
}
