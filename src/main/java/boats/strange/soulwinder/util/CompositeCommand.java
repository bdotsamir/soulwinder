package boats.strange.soulwinder.util;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public abstract class CompositeCommand extends Command {

  // whether the console can run this command
  private boolean playerOnly;
  // whether you need the permission for the command in order to *run* the command
  private boolean isPermissionRequired = true; // true by default

  private boolean isHidden = false;

  protected CompositeCommand(@NotNull String name, @NotNull String description, @NotNull String usageMessage, @NotNull List<String> aliases) {
    super(name, description, usageMessage, aliases);
  }

  @Override
  public boolean execute(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
    this.setup(); // Call the setup method which brings in permissions and such

    if(!(sender instanceof Player) && this.playerOnly) {
      sender.sendMessage(MCResponse.error("Only a player can run this command."));
      return false;
    }

    if (this.getPermission() != null
            && !sender.hasPermission(this.getPermission())
            && !sender.isOp()
            && this.isPermissionRequired()) {
      sender.sendMessage(MCResponse.error("You do not have access to this command!"));
      return false;
    }
    return this.call(sender, args);
  }

  /**
   * Permission setup and such goes here
   */
  public abstract void setup();

  public abstract boolean call(@NotNull CommandSender sender, @NotNull String[] args);

  public boolean isPlayerOnly() {
    return playerOnly;
  }

  public void setPlayerOnly(boolean playerOnly) {
    this.playerOnly = playerOnly;
  }

  public boolean isPermissionRequired() {
    return isPermissionRequired;
  }

  public void setPermissionRequired(boolean permissionRequired) {
    isPermissionRequired = permissionRequired;
  }

  public boolean getIsHidden() {
    return this.isHidden;
  }

  public void setHidden(boolean val) {
    this.isHidden = val;
  }

}
