package boats.strange.soulwinder.util;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;

public class MCResponse {

  public static NamedTextColor INFO = NamedTextColor.WHITE;
  public static NamedTextColor SUCCESS = NamedTextColor.GREEN;
  public static NamedTextColor ERROR = NamedTextColor.RED;
  public static NamedTextColor ACCENT = NamedTextColor.GOLD;

  public static Component error(String message) {
    return getPrefix()
            .append(Component.text(message, ERROR));
  }

  public static Component info(String message) {
    return getPrefix()
            .append(Component.text(message, INFO));
  }

  // alias. because i'll likely forget that it's named "info" and not "log"
  public static Component log(String message) {
    return info(message);
  }

  public static Component success(String message) {
    return getPrefix()
            .append(Component.text(message, SUCCESS));
  }

  public static Component getPrefix() {
    return Component.text("[", ACCENT)
            .append(Component.text("Soulwinder", INFO))
            .append(Component.text("] ", ACCENT));

  }

}
