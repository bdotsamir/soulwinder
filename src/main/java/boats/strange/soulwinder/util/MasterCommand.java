package boats.strange.soulwinder.util;

import boats.strange.soulwinder.commands.*;
import boats.strange.soulwinder.Soulwinder;
import boats.strange.soulwinder.commands.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// This is the master command that will house all the subcommands.
public class MasterCommand extends Command {

  private final HashMap<String, CompositeCommand> subCommands = new HashMap<>();
  // alias -> actual command name
  private final HashMap<String, String> aliases = new HashMap<>();

  public MasterCommand(@NotNull Soulwinder plugin) {
    super("sw");

//    this.registerCommand(new CreateField(plugin));
//    this.registerCommand(new DeleteField(plugin));
    this.registerCommand(new Dismiss(plugin));
    this.registerCommand(new Summon(plugin));
    this.registerCommand(new SkillTree(plugin));
//    this.registerCommand(new SetClassLetter(plugin));
//    this.registerCommand(new StudentInfo(plugin));
    this.registerCommand(new DebugDump(plugin));

    this.registerCommand(new Help(plugin, subCommands));

    plugin.getLogger().info("Registered all subcommands.");
  }

  private void registerCommand(CompositeCommand command) {
    this.subCommands.put(command.getName(), command);
    for(String alias : command.getAliases()) {
      this.aliases.put(alias, command.getName());
    }
  }

  @Override
  public boolean execute(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
    String subcommandString;
    try {
      subcommandString = args[0];
    } catch (IndexOutOfBoundsException e) {
      this.subCommands.get("help").execute(sender, label, args);
      return true;
    }

    CompositeCommand subcommand = this.subCommands.get(subcommandString);
    if(subcommand == null) {
      CompositeCommand aliasedCommand = this.subCommands.get(this.aliases.get(subcommandString));
      if(aliasedCommand != null) {
        subcommand = aliasedCommand;
      } else {
        sender.sendMessage(MCResponse.error("Unknown command."));
        return false;
      }
    }

    String[] actualArgs = new String[args.length - 1];
    System.arraycopy(args, 1, actualArgs, 0, args.length - 1);

    return subcommand.execute(sender, label, actualArgs);
  }

  @Override
  public @NotNull List<String> tabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args) {
    if(args.length == 1) {
      return new ArrayList<>(this.subCommands.keySet());
    } else {
      CompositeCommand subcommand = this.subCommands.get(args[0]);
      if(subcommand == null) {
        CompositeCommand aliasedCommand = this.subCommands.get(this.aliases.get(args[0]));
        if(aliasedCommand != null) {
          subcommand = aliasedCommand;
        } else {
          return new ArrayList<>();
        }
      }

      String[] actualArgs = new String[args.length - 1];
      System.arraycopy(args, 1, actualArgs, 0, args.length - 1);

      return subcommand.tabComplete(sender, alias, actualArgs);
    }
  }

  public HashMap<String, CompositeCommand> getSubCommands() {
    return this.subCommands;
  }
}
