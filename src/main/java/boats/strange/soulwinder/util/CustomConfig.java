package boats.strange.soulwinder.util;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;

public class CustomConfig {

  private final String fileName;
  private final JavaPlugin plugin;

  private final File file;
  private FileConfiguration fileConfiguration;

  public CustomConfig(@NotNull JavaPlugin plugin, @NotNull String fileName, boolean isInternal) {
    this.fileName = fileName;
    this.plugin = plugin;

    if(isInternal) {
      file = new File(plugin.getDataFolder(), ".internal/" + fileName);
    } else {
      file = new File(plugin.getDataFolder(), fileName);
    }
  }

  public void reloadConfig() {
    fileConfiguration = YamlConfiguration.loadConfiguration(file);

    InputStream defConfigStream = plugin.getResource(fileName);
    if(defConfigStream != null) {
      YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(new InputStreamReader(defConfigStream));
      fileConfiguration.setDefaults(defConfig);
    }
  }

  public FileConfiguration getConfig() {
    if(fileConfiguration == null) {
      this.reloadConfig();
    }

    return fileConfiguration;
  }

  public void saveConfig() {
    if(fileConfiguration == null || file == null) {
      return;
    }

    try {
      getConfig().save(file);
    } catch(Exception e) {
      this.plugin.getLogger().log(Level.SEVERE, "Could not save config to " + file, e);
    }
  }

}
