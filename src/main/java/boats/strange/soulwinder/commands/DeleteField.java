package boats.strange.soulwinder.commands;

import boats.strange.soulwinder.Soulwinder;
import boats.strange.soulwinder.GameObjects.PlayField;
import boats.strange.soulwinder.util.CompositeCommand;
import boats.strange.soulwinder.util.MCResponse;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

@Deprecated
public class DeleteField extends CompositeCommand {

    public static final String name = "deletefield";
    private static final String[] aliases = {"destroyfield", "df"};

    private final Soulwinder plugin;

    public DeleteField(Soulwinder plugin) {
        super(name, "Deletes your summoning field", "", Arrays.asList(aliases));

        this.plugin = plugin;
    }

    @Override
    public void setup() {
        this.setPermission("bat.teacher.deletefield");
        this.setPlayerOnly(true);
    }

    @Override
    public boolean call(@NotNull CommandSender sender, @NotNull String[] args) {
        PlayField field = this.plugin.getPlayerToFieldMap().get((Player) sender);

        if(field == null) {
            sender.sendMessage(MCResponse.error("You do not have a field to delete."));
            return false;
        }

        field.destroy();
        sender.sendMessage(MCResponse.success("Deleted field."));

        return true;
    }
}
