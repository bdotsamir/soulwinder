package boats.strange.soulwinder.commands;

import boats.strange.soulwinder.GameObjects.PlayField;
import boats.strange.soulwinder.Soulwinder;
import boats.strange.soulwinder.util.CompositeCommand;
import boats.strange.soulwinder.util.MCResponse;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

@Deprecated
public class CreateField extends CompositeCommand {

  public static final String name = "createfield";
  private static final String[] aliases = {"cf"};

  private final Soulwinder plugin;
  public CreateField(Soulwinder plugin) {
    super(name, "Creates a new game field", "", Arrays.asList(aliases));

    this.plugin = plugin;
  }

  @Override
  public void setup() {
    this.setPermission("bat.teacher.createfield");
    this.setPlayerOnly(true);
  }

  @Override
  public boolean call(@NotNull CommandSender sender, @NotNull String[] args) {
    Player player = (Player) sender;

    if(this.plugin.getPlayerToFieldMap().containsKey(player)) {
      sender.sendMessage(MCResponse.error("You already have a field!"));
      return false;
    }

    int xLength = 10; // x
    int height = 5; // y
    int zLength = 10; // z

    PlayField field = new PlayField(this.plugin, player.getLocation(), xLength, height, zLength);
    field.create(player);

    this.plugin.getPlayerToFieldMap().put(player, field);

    sender.sendMessage(MCResponse.success("Created field."));
    return true;
  }

//  @Override
//  public ArrayList<String> tabCompletion

}
