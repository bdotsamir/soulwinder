package boats.strange.soulwinder.commands;

import boats.strange.soulwinder.Soulwinder;
import boats.strange.soulwinder.util.CompositeCommand;
import boats.strange.soulwinder.util.MCResponse;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashMap;

public class Help extends CompositeCommand {

  public static final String name = "help";
  private static final String[] aliases = {"h", "?", "commands", "cmds", "halp", "hilfe"};

  private final HashMap<String, CompositeCommand> commands;

  public Help(@NotNull Soulwinder plugin, HashMap<String, CompositeCommand> commands) {
    super(name, "List all commands", "", Arrays.asList(aliases));

    this.commands = commands;
  }

  @Override
  public void setup() {
    this.setPermission("bat.help");
    this.setPlayerOnly(false);
    this.setPermissionRequired(false);
  }

  @Override
  public boolean call(@NotNull CommandSender sender, @NotNull String[] args) {
    Component helpMessage = MCResponse.info("Commands: \n\n");

    boolean showHidden = Arrays.asList(args).contains("-h");

    for(CompositeCommand command : this.commands.values()) {
      // If the sender doesn't have permission to use the command, skip it.
      if(!command.testPermission(sender) && command.isPermissionRequired()) continue;

      // If the command is "hidden" and we're not showing hidden commands,
      if(command.getIsHidden() && !showHidden) {
        continue;
      }

      helpMessage = helpMessage
              .append(Component.text("/bat " + command.getName(), command.getIsHidden() ? NamedTextColor.GOLD : NamedTextColor.AQUA))
              .append(Component.text(" - ", NamedTextColor.GRAY))
              .append(Component.text(command.getDescription(), NamedTextColor.WHITE))
              .append(Component.text("\n", NamedTextColor.WHITE));
    }

    sender.sendMessage(helpMessage);

    return true;
  }
}
