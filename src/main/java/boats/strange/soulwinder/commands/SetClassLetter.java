package boats.strange.soulwinder.commands;

import boats.strange.soulwinder.GameObjects.Student;
import boats.strange.soulwinder.Soulwinder;
import boats.strange.soulwinder.util.CompositeCommand;
import boats.strange.soulwinder.util.MCResponse;
import net.kyori.adventure.text.Component;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

@Deprecated
public class SetClassLetter extends CompositeCommand {

  private final Soulwinder plugin;

  public SetClassLetter(Soulwinder plugin) {
    super("setclassletter", "Set the class letter of a student", "/bat <command> <new letter> [player]", new ArrayList<>());

    this.plugin = plugin;
  }

  @Override
  public void setup() {
    this.setPermission("bat.admin.setclassletter");
    this.setPermissionRequired(true);
    this.setPlayerOnly(false);
  }

  @Override
  public boolean call(@NotNull CommandSender sender, @NotNull String[] args) {

    String classLetter;
    try {
      classLetter = args[0];
    } catch (IndexOutOfBoundsException e) {
      sender.sendMessage(MCResponse.error("Please specify a class letter, or \"unset\" to unset the class letter."));
      return false;
    }

    Player player;
    try {
      String playerName = args[1];
      player = this.plugin.getServer().getPlayer(playerName);
    } catch (IndexOutOfBoundsException e) {
      player = (Player) sender;
    }
    assert player != null;

    Student student = this.plugin.students.get(player);
    if (student == null) {
      sender.sendMessage(MCResponse.error("Could not find a student with that name."));
      return false;
    }

    switch (classLetter.toLowerCase()) {
      case "unset" -> student.setClassLetter(Student.ClassLetter.UNSET);
      case "a" -> student.setClassLetter(Student.ClassLetter.CLASS_A);
      case "b" -> student.setClassLetter(Student.ClassLetter.CLASS_B);
      case "c" -> student.setClassLetter(Student.ClassLetter.CLASS_C);
      case "d" -> student.setClassLetter(Student.ClassLetter.CLASS_D);
      case "e" -> student.setClassLetter(Student.ClassLetter.CLASS_E);
      case "f" -> student.setClassLetter(Student.ClassLetter.CLASS_F);
      default -> {
        sender.sendMessage(MCResponse.error("Please specify a valid class letter (A-F) or \"unset\" to unset the class letter."));
        return false;
      }
    }

    // Save the student data. Just in case.
    student.save();

    sender.sendMessage(MCResponse.success(student.player.getName() + "'s class letter has been set to ")
            .append(Component.text(classLetter.toUpperCase(), Student.colorToTextColor(student.getClassColor())))
    );

    if (!sender.equals(player)) {
      player.sendMessage(MCResponse.info("Your class letter has been set to ")
              .append(Component.text(classLetter.toUpperCase(), Student.colorToTextColor(student.getClassColor())))
      );
    }

    return true;
  }

  @Override
  public @NotNull List<String> tabComplete(@NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args) {
    if (args.length == 1) {
      return List.of("A", "B", "C", "D", "E", "F", "unset");
    } else if (args.length == 2) {
      return this.plugin.students.keySet().stream().map(Player::getName).toList();
    } else {
      return List.of();
    }
  }

}
