package boats.strange.soulwinder.commands;

import boats.strange.soulwinder.GameObjects.Avatar;
import boats.strange.soulwinder.GameObjects.PlayField;
import boats.strange.soulwinder.GameObjects.Student;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import com.sk89q.worldguard.protection.regions.RegionQuery;
import boats.strange.soulwinder.Soulwinder;
import boats.strange.soulwinder.util.CompositeCommand;
import boats.strange.soulwinder.util.MCResponse;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public class Summon extends CompositeCommand {

  public static final String name = "summon";
  private static final String[] aliases = {"summonavatar", "summonme", "s"};

  private final Soulwinder plugin;

  public Summon(Soulwinder plugin) {
    super(name, "Summon your avatar", "", Arrays.asList(aliases));

    this.plugin = plugin;
  }

  @Override
  public void setup() {
    this.setPermission("bat.summon");
    this.setPlayerOnly(true);
  }

  @Override
  public boolean call(@NotNull CommandSender sender, @NotNull String[] args) {
    Player player = (Player) sender;

    Location playerLocation = player.getLocation();
//    PlayField fieldPlayerIsIn = null;

//    // To summon an avatar, we need to make sure the player is first inside a
//    // worldguard-defined region.
//    RegionContainer container = this.plugin.getWorldGuard().getPlatform().getRegionContainer();
//    RegionQuery query = container.createQuery();
//    ApplicableRegionSet set = query.getApplicableRegions(BukkitAdapter.adapt(playerLocation));
//    for(ProtectedRegion region : set) {
//      String regionName = region.getId();
//      if(regionName.contains("playfield")) {
//        PlayField field = this.plugin.getRegionToFieldMap().get(region);
//        if(field == null) {
//          sender.sendMessage(MCResponse.error("Unexpected error while finding summoning field."));
//          return false;
//        }
//        fieldPlayerIsIn = field;
//        break;
//      }
//    }
//
//    if(fieldPlayerIsIn == null) {
//      sender.sendMessage(MCResponse.error("You are not inside of a summoning field!"));
//      return false;
//    }

    Location spawnLocation;
    // Spawn an armor stand directly in front of the player
    String currentCardinalDirection = getCardinalDirection(player);
    assert currentCardinalDirection != null;
    switch (currentCardinalDirection) {
      case "N" -> spawnLocation = playerLocation.add(0, 0, -1);
      case "NE" -> spawnLocation = playerLocation.add(1, 0, -1);
      case "E" -> spawnLocation = playerLocation.add(1, 0, 0);
      case "SE" -> spawnLocation = playerLocation.add(1, 0, 1);
      case "S" -> spawnLocation = playerLocation.add(0, 0, 1);
      case "SW" -> spawnLocation = playerLocation.add(-1, 0, 1);
      case "W" -> spawnLocation = playerLocation.add(-1, 0, 0);
      case "NW" -> spawnLocation = playerLocation.add(-1, 0, -1);
      default -> {
        player.sendMessage(MCResponse.error("Unexpected error while finding current direction."));
        return false;
      }
    }

//    if(fieldPlayerIsIn.getAvatars().containsKey(player)) {
//      sender.sendMessage(MCResponse.error("You already have an avatar summoned!"));
//      return false;
//    }

    Student student = this.plugin.students.get(player);
    assert student != null;

    if(student.getClassLetter() == Student.ClassLetter.UNSET) {
      sender.sendMessage(MCResponse.error("You do not have a class yet!"));
      return false;
    }

    Avatar avatar = new Avatar(student, spawnLocation);
    avatar.summon();

//    fieldPlayerIsIn.addAvatar(player, avatar);

    sender.sendMessage(MCResponse.success("Summoned avatar."));

    return true;
  }

  public static @Nullable String getCardinalDirection(Player player) {
    double rotation = (player.getLocation().getYaw() + 180.0F) % 360.0F;
    if (rotation < 0.0D) {
      rotation += 360.0D;
    }
    if ((0.0D <= rotation) && (rotation < 22.5D)) {
      return "N";
    }
    if ((22.5D <= rotation) && (rotation < 67.5D)) {
      return "NE";
    }
    if ((67.5D <= rotation) && (rotation < 112.5D)) {
      return "E";
    }
    if ((112.5D <= rotation) && (rotation < 157.5D)) {
      return "SE";
    }
    if ((157.5D <= rotation) && (rotation < 202.5D)) {
      return "S";
    }
    if ((202.5D <= rotation) && (rotation < 247.5D)) {
      return "SW";
    }
    if ((247.5D <= rotation) && (rotation < 292.5D)) {
      return "W";
    }
    if ((292.5D <= rotation) && (rotation < 337.5D)) {
      return "NW";
    }
    if ((337.5D <= rotation) && (rotation < 360.0D)) {
      return "N";
    }
    else return null;
  }

}
