package boats.strange.soulwinder.commands;

import boats.strange.soulwinder.GameObjects.Student;
import boats.strange.soulwinder.Soulwinder;
import boats.strange.soulwinder.util.CompositeCommand;
import boats.strange.soulwinder.util.MCResponse;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.List;

@Deprecated
public class StudentInfo extends CompositeCommand {

  private final Soulwinder plugin;

  public StudentInfo(Soulwinder plugin) {
    super("studentinfo", "Get information about a student", "/bat <command> [student]", List.of("student"));
    this.plugin = plugin;
  }

  @Override
  public void setup() {
    this.setPermission("bat.studentinfo");
    this.setPermissionRequired(true);
  }

  @Override
  public boolean call(@NotNull CommandSender sender, @NotNull String[] args) {
    Player player;
    try {
      String playerName = args[0];
      player = this.plugin.getServer().getPlayer(playerName);
    } catch (IndexOutOfBoundsException e) {
      player = (Player) sender;
    }

    if(player == null) {
      sender.sendMessage(MCResponse.error("Could not find a student with that name."));
      return false;
    }

    Student student = this.plugin.students.get(player);

    String classLetter = Student.classLetterToString(student.getClassLetter());
    // Join the list of skills with a comma and space
    String skills = String.join(", ", student.getSkills());

    TextColor classColor = !classLetter.equals("unset")
            ? Student.colorToTextColor(student.getClassColor())
            : NamedTextColor.RED;

    Component message = MCResponse.info("Student info for " + player.getName() + ":")
            .append(Component.text("\nClass Letter: ", NamedTextColor.AQUA))
            .append(Component.text(classLetter, classColor))
            .append(Component.text("\nSkills: ", NamedTextColor.AQUA))
            .append(Component.text(skills, NamedTextColor.GOLD))
            .append(Component.text("\n\nUUID: ", NamedTextColor.AQUA))
            .append(Component.text(player.getUniqueId().toString(), NamedTextColor.GOLD));

    sender.sendMessage(message);

    return true;
  }
}
