package boats.strange.soulwinder.commands;

import boats.strange.soulwinder.GameObjects.Avatar;
import boats.strange.soulwinder.GameObjects.PlayField;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import com.sk89q.worldguard.protection.regions.RegionQuery;
import boats.strange.soulwinder.Soulwinder;
import boats.strange.soulwinder.util.CompositeCommand;
import boats.strange.soulwinder.util.MCResponse;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class Dismiss extends CompositeCommand {

  public static final String name = "dismiss";
  private static final String[] aliases = {"dismissavatar", "dismissme"};

  private final Soulwinder plugin;

  public Dismiss(Soulwinder plugin) {
    super(name, "Dismisses your avatar", "", Arrays.asList(aliases));
    this.plugin = plugin;
  }

  @Override
  public void setup() {
    this.setPermission("bat.summon"); // Yes, I mean summon.
    // It wouldn't make sense if the player could only summon, but not dismiss, their avatar.
    this.setPlayerOnly(true);
  }

  @Override
  public boolean call(@NotNull CommandSender sender, @NotNull String[] args) {
    Player player = (Player) sender;

    PlayField fieldPlayerIsIn = null;

    // To dismiss an avatar, we need to make sure the player is first inside a
    // WorldGuard-defined region.
    RegionContainer container = this.plugin.getWorldGuard().getPlatform().getRegionContainer();
    RegionQuery query = container.createQuery();
    ApplicableRegionSet set = query.getApplicableRegions(BukkitAdapter.adapt(player.getLocation()));
    for(ProtectedRegion region : set) {
      String regionName = region.getId();
      if(regionName.contains("playfield")) {
        PlayField field = this.plugin.getRegionToFieldMap().get(region);
        if(field == null) {
          sender.sendMessage(MCResponse.error("Unexpected error while finding summoning field."));
          return false;
        }
        fieldPlayerIsIn = field;
        break;
      }
    }
    if(fieldPlayerIsIn == null) {
      sender.sendMessage(MCResponse.error("You're not currently inside of a summoning field!"));
      return false;
    }

    Avatar avatar = fieldPlayerIsIn.getAvatars().get(player);
    if(avatar == null) {
      sender.sendMessage(MCResponse.error("You haven't summoned your avatar!"));
      return false;
    }

    avatar.destroy();
    sender.sendMessage(MCResponse.success("Dismissed avatar."));

    fieldPlayerIsIn.getAvatars().remove(player);

    return true;
  }
}
