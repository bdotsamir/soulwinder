package boats.strange.soulwinder.commands;

import boats.strange.soulwinder.GameObjects.SkillTree.SkillTreeInventory;
import boats.strange.soulwinder.GameObjects.Student;
import boats.strange.soulwinder.Soulwinder;
import boats.strange.soulwinder.util.CompositeCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class SkillTree extends CompositeCommand {

  private final Soulwinder plugin;

  public SkillTree(Soulwinder plugin) {
    super("skilltree", "Interact with the skill tree", "", Arrays.asList("st", "skills"));
    this.plugin = plugin;
  }

  @Override
  public void setup() {
    this.setPermission("bat.skilltree");
    this.setPermissionRequired(true);
  }

  @Override
  public boolean call(@NotNull CommandSender sender, @NotNull String[] args) {
    Player player = (Player) sender;
    Student student = this.plugin.students.get(player);

    SkillTreeInventory stInv = new SkillTreeInventory(this.plugin, student);

    player.openInventory(stInv.getInventory());

    return true;
  }
}
