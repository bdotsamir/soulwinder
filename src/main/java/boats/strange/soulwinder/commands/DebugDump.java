package boats.strange.soulwinder.commands;

import boats.strange.soulwinder.GameObjects.PlayField;
import boats.strange.soulwinder.Soulwinder;
import boats.strange.soulwinder.util.CompositeCommand;
import boats.strange.soulwinder.util.MCResponse;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.logging.Level;

public class DebugDump extends CompositeCommand {

  public static final String name = "debugdump";
  public static final String[] aliases = {};

  private final Soulwinder plugin;

  public DebugDump(Soulwinder plugin) {
    super(name, "Dump debug values to console", "<command>", Arrays.asList(aliases));
    this.plugin = plugin;
  }

  @Override
  public void setup() {
    this.setPermission("bat.debugdump");
    this.setPermissionRequired(true);
  }

  @Override
  public boolean call(@NotNull CommandSender sender, @NotNull String[] args) {
    this.plugin.getLogger().setLevel(Level.FINE);
    this.plugin.getPlayerToFieldMap().values().forEach(field -> {
      String serialization = field.serialize();
      this.plugin.getLogger().fine(serialization);
    });

    // Should probably enable this.
    // this.plugin.getLogger().setLevel(Level.INFO);

    sender.sendMessage(MCResponse.info("Debug dump complete. Check the console for the results."));

    return true;
  }

  @Override
  public boolean getIsHidden() {
    return true;
  }
}
