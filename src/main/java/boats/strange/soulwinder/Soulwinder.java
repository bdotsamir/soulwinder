package boats.strange.soulwinder;

import boats.strange.soulwinder.GameObjects.PlayField;
import boats.strange.soulwinder.GameObjects.Student;
import boats.strange.soulwinder.events.InventoryClick;
import boats.strange.soulwinder.events.PlayerJoin;
import boats.strange.soulwinder.util.CustomConfig;
import boats.strange.soulwinder.util.MasterCommand;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.google.common.io.CharStreams;
import com.google.common.io.Files;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.command.CommandMap;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

public final class Soulwinder extends JavaPlugin {

  private ProtocolManager protocolManager;
  private WorldGuard wgInstance;

  private final HashMap<Player, PlayField> playerToFieldMap = new HashMap<>();
  private final HashMap<ProtectedRegion, PlayField> regionToFieldMap = new HashMap<>();

  public final Map<Player, Student> students = new HashMap<>();

  @Override
  public void onLoad() {
    this.protocolManager = ProtocolLibrary.getProtocolManager();
  }

  @Override
  public void onEnable() {
    // Plugin startup logic
    this.getLogger().info("onEnable called!");

    CommandMap commandMap = this.getServer().getCommandMap();
    commandMap.register("bakaandtest", new MasterCommand(this));

    this.wgInstance = WorldGuard.getInstance();

    CustomConfig exposedConfig = new CustomConfig(this, "customConfig.yml", false);
    exposedConfig.getConfig().options().copyDefaults(true);
    exposedConfig.getConfig().addDefault("foo", "bar");
    exposedConfig.saveConfig();

    // Register the inventory click event
    this.getServer().getPluginManager().registerEvents(new InventoryClick(this), this);
    this.getServer().getPluginManager().registerEvents(new PlayerJoin(this), this);

    this.copyInternalReadme();
  }

  @Override
  public void onDisable() {
    // Plugin shutdown logic
    this.getLogger().info("onDisabled called!");
    this.getLogger().info("Destroying remaining fields...");

    // Destroy the remaining fields
    for (Player player : this.playerToFieldMap.keySet()) {
      PlayField field = this.playerToFieldMap.get(player);
      field.destroy();
    }

    // Save all student data
    for(Student student : this.students.values()) {
      student.save();
    }
  }

  public ProtocolManager getProtocolManager() {
    return this.protocolManager;
  }

  public WorldGuard getWorldGuard() {
    return this.wgInstance;
  }

  public HashMap<Player, PlayField> getPlayerToFieldMap() {
    return this.playerToFieldMap;
  }

  public HashMap<ProtectedRegion, PlayField> getRegionToFieldMap() {
    return this.regionToFieldMap;
  }

  private void copyInternalReadme() {
    // Create a new directory
    File internalDir = new File(this.getDataFolder(), ".internal");
    if (!internalDir.exists()) {
      boolean mkdirSuccessful = internalDir.mkdir();
      if(!mkdirSuccessful) {
        throw new RuntimeException("Failed to create .internal directory!");
      }
    }

    Reader internalReadmeWarning = this.getTextResource("README.txt");
    if (internalReadmeWarning == null) {
      throw new RuntimeException("README.txt not found!");
    }
    try {
      byte[] bytes = CharStreams.toString(internalReadmeWarning).getBytes();
      File internalReadmeFile = new File(this.getDataFolder(), ".internal/README.txt");
      Files.write(bytes, internalReadmeFile);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
